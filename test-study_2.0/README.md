# Test study

The test study for Sandbox based on DataPortal 2.0 has to be in TEST directory:
```/europdx/data/UPDOG/TEST```
(the TEST directory had to be hard coded in DataPortal Loader's code).

However - the name of the study need not to be just "TEST" (can be e.g. "NKIXY" etc.)

Except for the files in TEST folder there has to be also ```/europdx/diagnosis_mapping.json``` file provided.
This file maps the cancer type specified in the .tsv files to the official NCI Thesaurus cancer type (see http://www.ontobee.org/ontology/NCIT)


### File diagnosis_mapping.json - example + comments
See in ```// comments``` below which values should be put into diagnosis_mapping.json file

```
{
  "mappings": [
    {
      "entityId": 1565,
      "entityType": "diagnosis",
      "mappingLabels": [
                "DataSource",
                "SampleDiagnosis",
                "OriginTissue",
                "TumorType"
      ],
      "mappingValues": {
        "OriginTissue": "rectum", // [1] TEST_metadata-sample.tsv column J
        "TumorType": "primary", // [2] TEST_metadata-sample.tsv column I
        "SampleDiagnosis": "colorectal carcinoma", // [3] TEST_metadata-sample.tsv column H
        "DataSource": "nkixy" // [4] TEST_metadata-loader.tsv colummn C
      },
      "mappedTermLabel": "COLORECTAL CARCINOMA",
      "mappedTermUrl": "http:\/\/purl.obolibrary.org\/obo\/NCIT_C2955", // Here to be specified the cancer type by url address give by http://www.ontobee.org/ontology/NCIT
      "mapType": "DIRECT",
      "justification": "0",
      "status": "Created",
      "suggestedMappings": [
      ],
      "mappingKey": "diagnosis__nkixy__colorectal carcinoma__rectum__primary" // mappingKey to be composed like: diagnosis__[4]__[3]__[1]__[2] // see above
    }
  ]
}
```
